docker run -d --name gitlab-runner --restart always -v /home/douglas/ci-cd/gitlab/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

ssh-keygen -t rsa -b 2048

curl -X POST --data-urlencode "payload={\"channel\": \"#bytebank\", \"username\": \"webhookbot\", \"text\": \"Deu tudo certo na pipeline!\", \"icon_emoji\": \":ghost:\"}" https://hooks.slack.com/services/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
